# Sandstorm

Sandstorm-ipv6 is a soft fork of [sandstorm](https://github.com/sandstorm-io/sandstorm) that can fetch its dependencies on an IPV6-only server.

Sandstorm is a self-hostable web productivity suite. It's implemented as a security-hardened web app package manager. Sandstorm makes it easy to run your own server.
